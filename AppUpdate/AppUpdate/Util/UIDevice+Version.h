//
// Created by Marko Cicak on 7/15/16.
// Copyright (c) 2016 codecentric. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIDevice (Version)

+ (float) currentVersion;

@end
