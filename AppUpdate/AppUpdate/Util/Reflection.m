//
// Created by Marko Cicak on 7/15/16.
// Copyright (c) 2016 codecentric. All rights reserved.
//

#import <objc/runtime.h>
#import "Reflection.h"

@implementation Reflection

+ (NSArray<Class>*) classesImplementingProtocol:(Protocol*)p
{
    NSMutableArray* ret = [NSMutableArray array];

    Class* classes = NULL;
    int numClasses = objc_getClassList(NULL, 0);
    if (numClasses > 0)
    {
        classes = (__unsafe_unretained Class*) malloc(sizeof(Class) * numClasses);
        numClasses = objc_getClassList(classes, numClasses);
        for (int index = 0; index < numClasses; index++)
        {
            Class nextClass = classes[index];
            if (class_conformsToProtocol(nextClass, p))
            {
                [ret addObject:nextClass];
            }
        }
    }
    return ret;
}

@end
