//
// Created by Marko Cicak on 7/15/16.
// Copyright (c) 2016 codecentric. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Reflection : NSObject

+ (NSArray<Class>*) classesImplementingProtocol:(Protocol*)p;

@end
