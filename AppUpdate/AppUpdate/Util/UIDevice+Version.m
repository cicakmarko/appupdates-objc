//
// Created by Marko Cicak on 7/15/16.
// Copyright (c) 2016 codecentric. All rights reserved.
//

#import "UIDevice+Version.h"

@implementation UIDevice (Version)

+ (float) currentVersion
{
    static float v = 0.0f;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        v = [[[UIDevice currentDevice] systemVersion] floatValue];
    });
    return v;
}

@end
