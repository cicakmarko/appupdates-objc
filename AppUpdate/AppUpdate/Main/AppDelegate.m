//
//  AppDelegate.m
//  AppUpdate
//
//  Created by Marko Cicak on 7/15/16.
//  Copyright © 2016 codecentric. All rights reserved.
//

#import "AppDelegate.h"
#import "AppUpdater.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL) application:(UIApplication*)application willFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
#if TARGET_IPHONE_SIMULATOR
    NSLog(@"Documents Directory: %@", [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
    inDomains:NSUserDomainMask] lastObject]);
#endif

    [AppUpdater performUpdateSteps];
    return YES;
}

- (BOOL) application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    return YES;
}

@end
