//
//  AppDelegate.h
//  AppUpdate
//
//  Created by Marko Cicak on 7/15/16.
//  Copyright © 2016 codecentric. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

