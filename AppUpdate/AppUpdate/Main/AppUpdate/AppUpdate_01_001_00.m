//
// Created by Marko Cicak on 7/15/16.
// Copyright (c) 2016 codecentric. All rights reserved.
//

#import "AppUpdate_01_001_00.h"


@implementation AppUpdate_01_001_00

- (NSString* const) version
{
    return @"001-001-000";
}

- (BOOL const) canExecuteUpdate
{
    return SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(9);
}

- (NSString* const) stepName
{
    return @"SpotlightIndexing";
}

- (NSString* const) stepDescription
{
    return @"Index documents and photos in spotlight.";
}

- (void (^)(void)) updateBlock
{
    return ^{
        // iterate through all documents and photos and index them in spotlight
    };
}

@end
