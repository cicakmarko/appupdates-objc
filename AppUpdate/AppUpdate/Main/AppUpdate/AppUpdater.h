//
// Created by Marko Cicak on 7/15/16.
// Copyright (c) 2016 codecentric. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppUpdater : NSObject

// Update steps executed before creating RestKit and CoreData stack and checking logged-in user state.
+ (void) performUpdateSteps;

@end
