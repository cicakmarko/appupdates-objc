//
// Created by Marko Cicak on 7/15/16.
// Copyright (c) 2016 codecentric. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AppUpdate <NSObject>

// Version in format ###-###-### with leading zeros. Used to sort updaters chronologically.
- (NSString* const) version;

// Could be that update is only for specific ios version thus should not be executed for older versions.
- (BOOL const) canExecuteUpdate;

// String in format upd-#_#_#. Needs to be unique across all app updaters.
- (NSString* const) stepName;

- (NSString* const) stepDescription;

- (void (^)(void)) updateBlock;

@end
