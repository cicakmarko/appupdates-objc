//
// Created by Marko Cicak on 7/15/16.
// Copyright (c) 2016 codecentric. All rights reserved.
//

#import "AppUpdater.h"
#import "AppUpdate.h"
#import "Reflection.h"


@implementation AppUpdater

+ (void) performUpdateSteps
{
    NSArray<Class>* updateClasses = [Reflection classesImplementingProtocol:@protocol(AppUpdate)];
    NSArray* updaters = ASTMap(updateClasses, (id (^)(id)) ^id(Class updateClass) {
        return updateClass.new;
    });

    updaters = ASTSort(updaters, ^NSComparisonResult(id <AppUpdate> obj1, id <AppUpdate> obj2) {
        return [obj1.version compare:obj2.version];
    });

    ASTEach(updaters, ^(id <AppUpdate> updater) {
        if ([updater canExecuteUpdate])
        {
            [self performUpdateStepWithUpdater:updater];
        }
    });
}

+ (void) performUpdateStepWithUpdater:(id <AppUpdate> const)updater
{
    if (![NSUserDefaults.standardUserDefaults objectForKey:updater.version])
    {
        NSLog(@"▸ Performing update step: %@", updater.stepName);
        updater.updateBlock();
        [NSUserDefaults.standardUserDefaults setValue:updater.stepDescription forKey:updater.version];
        NSLog(@"▸ Finished update step: %@", updater.stepName);
        [NSUserDefaults.standardUserDefaults synchronize];
    }
}

@end
