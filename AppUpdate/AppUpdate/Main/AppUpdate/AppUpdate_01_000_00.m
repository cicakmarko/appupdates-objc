//
// Created by Marko Cicak on 7/15/16.
// Copyright (c) 2016 codecentric. All rights reserved.
//

#import "AppUpdate_01_000_00.h"


@implementation AppUpdate_01_000_00

- (NSString* const) version
{
    return @"001-000-000";
}

- (BOOL const) canExecuteUpdate
{
    return YES;
}

- (NSString* const) stepName
{
    return @"FirstRun";
}

- (NSString* const) stepDescription
{
    return @"1strun";
}

- (void (^)(void)) updateBlock
{
    return ^{
        // clear keychain data if existed any
        NSLog(@"Clear keychain");
    };
}

@end
